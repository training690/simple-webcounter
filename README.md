# Webcounter

Simple Python Webcounter with redis 

## Build
docker build -t pt02220/webcounter:1.0.1 .

## Dependencies
docker run -d  --name redis --rm redis:alpine

## Run
docker run -d --rm -p80:5000 --name webcounter --link redis -e REDIS_URL=redis pt02220/webcounter:1.0.1

### Gitlab register
gitlab-runner register -n \
--url https://gitlab.com/ \
--executor shell \
--description "docker-lab" \
--tag-list "production" \
--registration-token GR1348941HHV3vsTqCK6JyncqWN8U